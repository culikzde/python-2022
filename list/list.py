import os, sys, time, shutil
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

# pythom -m pip install PyQt5

# use_crc = True
use_crc = False
if use_crc :
   import hashlib

# --------------------------------------------------------------------------

import os
import paramiko
from paramiko import SSHClient
from paramiko.config import SSHConfig
from scp import SCPClient

# dnf install python3-paramiko python3-scp
# python -m pip install scp

ssh_client = None
sch_client = None
directory = "temp/test0"
remote_name = "remote"

def run (ssh_client, cmd) :
    print (cmd)
    stdin, stdout, stderr = ssh_client.exec_command (cmd)
    for line in stderr.readlines () :
        print ("Error:", line, end="")
    for line in stdout.readlines () :
        print (line, end="")

def connect_to_remote () :
    global ssh_client
    global scp_client
    global directory
    global remote_name

    host = "pc" # name from config

    config = SSHConfig.from_path (os.path.expanduser ("./config"))
    # config = SSHConfig.from_path (os.path.expanduser ("~/.ssh/config"))
    conf = config.lookup (host)

    hostname = conf ["hostname"]
    username = conf ["user"]
    password = conf.get ("password", "")
    key_filename = conf.get ("identityfile", [""])[0]
    directory = conf.get ("directory", "temp/test0")

    ssh_client = SSHClient ()
    ssh_client.load_system_host_keys ()
    ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

    print ("connecting", hostname, "...")

    if key_filename != "" :
       ssh_client.connect (hostname, username = username, key_filename = key_filename)
    else :
       ssh_client.connect (hostname, username = username, password = password)

    scp_client = SCPClient (ssh_client.get_transport())

    remote_name = username + "@" + hostname + ":"
    """
    run (ssh_client, "mkdir -p " + directory)

    scp_client.put ("example.cc", os.path.join (directory, "example.cc"))

    run (ssh_client, "cd " + directory + " && " + "rm example.bin")

    run (ssh_client, "cd " + directory + " && " +
                    "gcc example.cc -o example.bin -lstdc++ " + " && "  +
                    "./example.bin")
     """

# --------------------------------------------------------------------------

class FileView (QTreeWidget) :
    def __init__ (self, parent = None) :
        super().__init__ (parent)
        self.setHeaderLabels (["name", "size", "date", "crc"])
        self.itemDoubleClicked.connect (self.onClick)

    def onClick (self, node, column) :
        if node.is_dir :
           path = node.path
           self.showDir (path)

    def showDir (self, path) :
        self.clear ()
        path = os.path.abspath (path)
        self.path = path

        node = QTreeWidgetItem (self)
        node.setText (0, "..")
        node.path = os.path.abspath (os.path.join (path, ".."))
        node.setToolTip (0, node.path)
        node.is_dir = True

        items = os.listdir (path)
        items.sort ()

        for name in items :
            node = QTreeWidgetItem (self)
            node.setText (0, name)

            file_name = os.path.join (path, name)
            node.setToolTip (0, file_name)
            node.path = file_name

            if os.path.isdir (file_name) :
               node.setForeground (0, QColor ("blue"))
               node.is_dir = True
            else :
               node.setForeground (0, QColor ("red"))
               node.is_dir = False

               "size"
               node.setText (1, str (os.path.getsize (file_name)))

               "time"
               t = os.path.getmtime (file_name)
               u = time.gmtime (t)
               s = time.strftime("%d-%m-%Y %H:%M:%S", u)
               node.setText (2, s)

               "crc"
               if use_crc :
                  try :
                     crc = hashlib.md5 ()
                     # crc = hashlib.sha512 ()
                     f = open (file_name, "rb")
                     while True :
                         data = f.read (32*1024)
                         if not data :
                             break
                         crc.update (data)
                     f.close ()
                     node.setText (3, crc.hexdigest())
                  except :
                      pass

    def showRemoteDir (self, path) :
        self.clear ()
        # path = os.path.abspath (path)
        self.path = path

        node = QTreeWidgetItem (self)
        node.setText (0, "..")
        node.path = path
        node.setToolTip (0, remote_name + node.path)
        node.is_dir = True


        # for name in dir (scp_client) :
        #     print (name)

        # items = scp_client.listdir (path)
        # items.sort ()

        items = []
        cmd = "cd " + directory + " && " + "ls -1"
        stdin, stdout, stderr = ssh_client.exec_command (cmd)
        for line in stdout.readlines () :
            print (line, end="")
            items.append (line.strip ())

        items.sort ()

        for name in items :
            node = QTreeWidgetItem (self)
            node.setText (0, name)

            file_name = os.path.join (path, name)
            node.setToolTip (0, remote_name + file_name)
            node.path = file_name

            """
            if os.path.isdir (file_name) :
               node.setForeground (0, QColor ("blue"))
               node.is_dir = True
            else :
               node.setForeground (0, QColor ("red"))
               node.is_dir = False

               "size"
               node.setText (1, str (os.path.getsize (file_name)))

               "time"
               t = os.path.getmtime (file_name)
               u = time.gmtime (t)
               s = time.strftime("%d-%m-%Y %H:%M:%S", u)
               node.setText (2, s)
            """

# --------------------------------------------------------------------------

class Window (QMainWindow) :

    def __init__ (self) :
        super().__init__ ()

        self.vsplitter = QSplitter (self)
        self.vsplitter.setOrientation (Qt.Vertical)
        self.setCentralWidget (self.vsplitter)

        self.hsplitter = QSplitter (self)

        self.left = FileView (self)
        self.right = FileView (self)

        self.hsplitter.addWidget (self.left)
        self.hsplitter.addWidget (self.right)

        self.cmdline = QLineEdit ()
        self.info = QTextEdit ()

        self.buttons = QWidget ()
        self.layout = QHBoxLayout (self.buttons)

        self.addButton ("&View", "F3", self.view)
        self.addButton ("&Edit", "F4")
        # self.addButton ("&Copy", "F5", self.copy)
        self.addButton ("&Copy", "F5", self.copyToRemote)
        self.addButton ("&Move", "F6")
        self.addButton ("Make Dir", "F7")
        self.addButton ("Delete", "F8")

        self.vsplitter.addWidget (self.hsplitter)
        self.vsplitter.addWidget (self.cmdline)
        self.vsplitter.addWidget (self.info)
        self.vsplitter.addWidget (self.buttons)

        self.vsplitter.setStretchFactor (0, 10)
        self.vsplitter.setStretchFactor (1, 1)
        self.vsplitter.setStretchFactor (2, 2)
        self.vsplitter.setStretchFactor (3, 1)

    def addButton (self, name, key, func = None) :
        b = QPushButton (self)
        b.setText (name + " (" + key + ")")
        b.setShortcut (key)
        if func != None :
           b.clicked.connect (func)
        self.layout.addWidget (b)

    def addInfo (self, txt) :
        self.info.append (txt)

    def view (self) :
        node = self.left.currentItem ()
        if node != None and not node.is_dir :

           with open (node.path) as f :
               lines = f.readlines()

           text = ""
           for line in lines :
               text = text + line

           edit = QTextEdit (None)
           edit.setPlainText (text)
           edit.show ()

           self.keep = edit

    def copy (self) :
        node = self.left.currentItem ()
        if node != None and not node.is_dir :
           source = node.path
           target = self.right.path
           self.addInfo ("Copy " + source + " to " + target)
           shutil.copy2 (source, target)
           self.right.showDir (target) # refresh

    def copyToRemote (self) :
        node = self.left.currentItem ()
        if node != None and not node.is_dir :
           source = node.text (0)
           target = self.right.path
           self.addInfo ("Copy " + source + " to " + remote_name + target)
           scp_client.put (source, os.path.join (target, source))
           self.right.showRemoteDir (target) # refresh

# --------------------------------------------------------------------------

os.makedirs ("left", exist_ok=True)
os.makedirs ("right", exist_ok=True)

app = QApplication (sys.argv)
win = Window ()
win.show ()
# win.left.showDir ("left")
# win.right.showDir ("right")
connect_to_remote ()
win.left.showDir (".")
win.right.showRemoteDir (directory)
sys.exit (app.exec_ ())
