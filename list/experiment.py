
import os
import paramiko
from paramiko import SSHClient
from paramiko.config import SSHConfig
from scp import SCPClient

# dnf install python3-paramiko python3-scp
# python -m pip install scp

def run (ssh_client, cmd) :
    print (cmd)
    stdin, stdout, stderr = ssh_client.exec_command (cmd)
    for line in stderr.readlines () :
        print ("Error:", line, end="")
    for line in stdout.readlines () :
        print (line, end="")

host = "pc" # name from config

config = SSHConfig.from_path (os.path.expanduser ("./config"))
# config = SSHConfig.from_path (os.path.expanduser ("~/.ssh/config"))
conf = config.lookup (host)

hostname = conf ["hostname"]
username = conf ["user"]
password = conf.get ("password", "")
key_filename = conf.get ("identityfile", [""])[0]
directory = conf.get ("directory", "temp/test0")

ssh_client = SSHClient ()
ssh_client.load_system_host_keys ()
ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

print ("connecting", hostname, "...")

if key_filename != "" :
   ssh_client.connect (hostname, username = username, key_filename = key_filename)
else :
   ssh_client.connect (hostname, username = username, password = password)


run (ssh_client, "mkdir -p " + directory)

scp_client = SCPClient (ssh_client.get_transport())
scp_client.put ("example.cc", os.path.join (directory, "example.cc"))

run (ssh_client, "cd " + directory + " && " + "rm example.bin")

run (ssh_client, "cd " + directory + " && " +
                 "gcc example.cc -o example.bin -lstdc++ " + " && "  +
                 "./example.bin")
