#!/bin/env python

import os, sys, time

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtSql import *
# pip install PyQt5

import csv
# dnf install libreoffice

import openpyxl
# dnf install python3-openpyxl
# python -m pip install openpyxl

data = [ [1, 2, 3], [10, 20, 30] ]

N = 10
data = N * [ N * [ 0 ] ]

data = [ ]
for i in range (N) :
    data.append ( [ i+1 + j+1 for j in range (N) ] )

# print (data)

if 0 :
    for i in range (N) :
        print ("[", end = "")
        for j in range (N) :
            print (data[i][j], ",", sep="", end="")
        print ("]")

# --------------------------------------------------------------------------

# import csv

def writeCSV (file_name, data) :
    with open (file_name, "w", newline="") as csv_file :
       writer = csv.writer (csv_file,
                            delimiter = ',',
                            quotechar = '"',
                            quoting = csv.QUOTE_NONNUMERIC)
       for block in data :
           writer.writerow (block)

writeCSV ("output.csv" , data)

# --------------------------------------------------------------------------

import openpyxl

def addSum (data) :

    lines = len (data)
    columns = len (data [0])

    # prazdna radka
    data.append (N * [""])

    block = []
    for col in range (columns) :
        column_name = chr (ord ("A") + col)
        sum = "=SUM(" + column_name + "1" + ":" + column_name + str (lines) + ")"
        block.append (sum)

    data.append (block)

def writeXSLX (file_name, data) :
    wb = openpyxl.Workbook ()
    ws = wb.active
    # ws.create_sheet ("name")

    line = 1
    for block in data :
        col = 1

        for item in block :
            c = ws.cell (row=line, column=col, value=item)

            "barva pisma"
            if line == col:
               color_name = "red"
            else :
               color_name = "blue"
            color_name = QColor (color_name).name()[1:]
            c.font = openpyxl.styles.Font (color = color_name)

            "barva podkladu"
            if line == N+2 :
               color_name = "yellow"
               color_name = QColor (color_name).name()[1:]
               c.fill = openpyxl.styles.PatternFill (
                   start_color = color_name,
                   end_color = color_name,
                   fill_type = "solid")

            col = col + 1
        line = line + 1

    chart = openpyxl.chart.BarChart ()
    chart.type = "col"
    chart.style = 10

    data_ref = openpyxl.chart.Reference (ws,
        min_col = 1,
        max_col = N,
        min_row = N+2,
        max_row = N+2)

    chart.add_data(data_ref, titles_from_data=False)

    chart.shape = 4
    ws.add_chart (chart, "E7")

    wb.save (file_name)

# --------------------------------------------------------------------------

class Window (QMainWindow) :

    def __init__ (self, parent = None) :
       super (Window, self).__init__ (parent)

       self.table = QTableWidget ()
       self.setCentralWidget (self.table)

    def displayData (self, data) :
        self.data = data

        # self.table.horizontalHeader().setStretchLastSection(True)

        titles = []
        block = data [0]

        self.table.setColumnCount (len (block))
        self.table.setRowCount (len (data))

        for block in data :
            col = 0
            for item in block :
                titles.append (chr (ord ('A') + col))
                col = col + 1
        self.table.setHorizontalHeaderLabels (titles)

        line = 0
        col = 0
        for item in block :
            node = QTableWidgetItem ()
            node.setText (str (item))
            if line == col :
                node.setForeground (QColor ("red"))
                node.setBackground (QColor ("yellow").lighter())
            else :
                node.setForeground (QColor ("blue"))
            node.setToolTip ("(" + str (line) + "," + str (col) + ")")
            self.table.setItem (line, col, node)
            col = col + 1
        line = line + 1

# --------------------------------------------------------------------------

class DbWindow (QMainWindow) :

    def __init__ (self, parent = None) :
       super (DbWindow, self).__init__ (parent)

       self.table = QTableView ()
       self.setCentralWidget (self.table)

    def displayData (self, data) :
        # from PyQt5.QtSql import *

        # QSqlDatabase db = QSqlDatabase.addDatabase ("QMYSQL")
        # db.setHostName("pc.domain")
        # db.setDatabaseName ("database_name")
        # db.setUserName("uzivatel")
        # db.setPassword("heslo")
        # ok = db.open();

        db = QSqlDatabase.addDatabase ("QSQLITE")
        # db.setDatabaseName (":memory:")
        db.setDatabaseName ("test.sqlite")
        ok = db.open ()
        print ("ok =", ok)

        query = db.exec ("DROP TABLE IF EXISTS cisla")

        query = db.exec ("CREATE TABLE IF NOT EXISTS cisla "
                         + " (line INTEGER, col INTEGER, value REAL)")

        # db.exec_ ("INSERT INTO cisla (line, col, value) VALUES (1, 2, 3)")
        # db.exec_ ("INSERT INTO cisla (line, col, value) VALUES (3, 4, 5)")

        insert = QSqlQuery (db)
        insert.prepare ("INSERT INTO cisla (line, col, value) "
                        "VALUES (:line, :col, :value)")

        line = 0
        for block in data :
            col = 0
            for item in block :
                insert.bindValue (":line", QVariant (line))
                insert.bindValue (":col", QVariant (col))
                insert.bindValue (":value", QVariant (item))
                insert.exec_ ()
                col = col + 1
            line = line + 1

        "zobrazeni v QTableView"

        model = QSqlTableModel (self, db)
        model.setTable ("cisla")
        model.select ()

        self.table.setModel (model)
        # self.model = model
        # self.remember = db
        # time.sleep (1000)



# --------------------------------------------------------------------------

if __name__ == "__main__" :

   writeCSV  ("output.csv" , data)

   addSum (data)
   writeXSLX ("output.xlsx" , data)
   # os.system ("oocalc output.xlsx")

   if 1 :
        app = QApplication (sys.argv)
        # win = Window ()
        win = DbWindow ()
        win.show ()
        win.displayData (data)
        app.exec_ ()

        os.system ("sqlite3 test.sqlite '.mode table' 'select * from cisla'")
        # os.system ("sqlite3 test.sqlite '.mode html' 'select * from cisla'")
