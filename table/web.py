#!/bin/env python

from socketserver import TCPServer
from http.server import SimpleHTTPRequestHandler

import sys, socket, json, urllib.request, optparse

options = optparse.OptionParser ()
options.add_option ("-p", "--port", dest="port", default = 1234, action="store", help="Web server TCP port number")
(opts, args) = options.parse_args ()
port = int (opts.port)

server = TCPServer (("", port), SimpleHTTPRequestHandler)
server.socket.setsockopt (socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

try :
   print ("serving at port", port)
   server.serve_forever ()
except KeyboardInterrupt :
   print ("Control C received, shutting down the web server")
finally :
   server.socket.close ()

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

